<?php
/**
 * @file
 * simple_conference_features.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function simple_conference_features_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-simple-conference-menu.
  $menus['menu-simple-conference-menu'] = array(
    'menu_name' => 'menu-simple-conference-menu',
    'title' => 'Simple Conference Menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Simple Conference Menu');


  return $menus;
}
