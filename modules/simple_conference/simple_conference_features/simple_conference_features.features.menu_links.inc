<?php
/**
 * @file
 * simple_conference_features.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function simple_conference_features_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-simple-conference-menu:<front>
  $menu_links['menu-simple-conference-menu:<front>'] = array(
    'menu_name' => 'menu-simple-conference-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-simple-conference-menu:sessions
  $menu_links['menu-simple-conference-menu:sessions'] = array(
    'menu_name' => 'menu-simple-conference-menu',
    'link_path' => 'sessions',
    'router_path' => 'sessions',
    'link_title' => 'Sessions',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-simple-conference-menu:speakers
  $menu_links['menu-simple-conference-menu:speakers'] = array(
    'menu_name' => 'menu-simple-conference-menu',
    'link_path' => 'speakers',
    'router_path' => 'speakers',
    'link_title' => 'Speakers',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-simple-conference-menu:sponsors
  $menu_links['menu-simple-conference-menu:sponsors'] = array(
    'menu_name' => 'menu-simple-conference-menu',
    'link_path' => 'sponsors',
    'router_path' => 'sponsors',
    'link_title' => 'Sponsors',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Home');
  t('Sessions');
  t('Speakers');
  t('Sponsors');


  return $menu_links;
}
