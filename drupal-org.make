; Simple Conference make file
core = "7.x"
api = "2"
; comment this in to use in local development
; projects[drupal][version] = "7.x"

; Modules
projects[ctools][version] = "1.2"
projects[ctools][subdir] = "contrib"

projects[context][version] = "3.0-beta6"
projects[context][subdir] = "contrib"

projects[date][version] = "2.6"
projects[date][subdir] = "contrib"

;projects[profiler_builder][version] = "1.0-rc2"
;projects[profiler_builder][subdir] = "contrib"

projects[features][version] = "1.0"
projects[features][subdir] = "contrib"

projects[appcache][version] = "1.x-dev"
projects[appcache][subdir] = "contrib"

projects[module_filter][version] = "1.7"
projects[module_filter][subdir] = "contrib"

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[token][version] = "1.4"
projects[token][subdir] = "contrib"

projects[relation][version] = "1.0-rc3"
projects[relation][subdir] = "contrib"

projects[views][version] = "3.5"
projects[views][subdir] = "contrib"

projects[webform][version] = "3.18"
projects[webform][subdir] = "contrib"

projects[transliteration][version] = "3.1"
projects[transliteration][subdir] = "contrib"

projects[path_breadcrumbs][version] = "2.0-beta17"
projects[path_breadcrumbs][subdir] = "contrib"


; Themes
; zen
projects[zen][type] = "theme"
projects[zen][version] = "5.1"
projects[zen][subdir] = "contrib"
; simple_conference_theme
projects[simple_conference_theme][type] = "theme"
projects[simple_conference_theme][subdir] = "simple_conference"
projects[simple_conference_theme][download][type] = git
projects[simple_conference_theme][download][url] = http://git.drupal.org/sandbox/typhonius/1898318.git
