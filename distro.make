; Use this file to build a full distribution including Drupal core and the
; Simple Conference install profile using the following command:
;
; drush make distro.make <target directory>

api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = "7"

; Add Simple Conference to the full distribution build.
projects[simple_conference][type] = profile
;projects[simple_conference][version] = 1.x-dev
projects[simple_conference][download][type] = git
projects[simple_conference][download][url] = http://git.drupal.org/sandbox/typhonius/1894182.git
projects[simple_conference][download][branch] = 7.x-1.x
